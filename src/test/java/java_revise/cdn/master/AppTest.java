package java_revise.cdn.master;

import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.Test;

interface SlowTests{}
interface SlowerTests extends SlowTests{}

public class AppTest {
    @Test
    @Category(SlowTests.class)
    public void testSlow() {
        System.out.println("slow");
    }

    @Test
    @Category(SlowerTests.class)
    public void testSlower() {
        System.out.println("slower");
    }

}
