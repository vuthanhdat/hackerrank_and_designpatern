package java_revise.cdn.master.codilityaxon;

import java.util.HashMap;
import java.util.Map;

public class Solution {
    static int solution(int[] ranks) {
        // write your code in Java SE 8
        int totalReporter = 0;
        Map<Integer, Integer> map = countOccurrences(ranks);
        System.out.println(map);
        for (Map.Entry<Integer, Integer> item : map.entrySet()){
            totalReporter += countReporter(map, item.getKey());
        }
        return totalReporter;
    }

    static Map<Integer, Integer> countOccurrences(int[] arr) {
        int len = arr.length;
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();

        for (int i = 0; i < len; i++) {
            int key = arr[i];
            if (map.containsKey(key)) {
                int value = map.get(key);
                map.put(key, value + 1);
            } else {
                map.put(key, 1);
            }
        }

        return map;
    }

    static int countReporter(Map<Integer, Integer> map, int val) {
        int reporter = 0;
        int receiver = val + 1;
        if(map.containsKey(receiver)){
            reporter = map.get(val);
        }
        return reporter;
    }

    public static void main(String[] args) {
        int []a = {4, 4, 3, 3, 1, 0};
        System.out.println(solution(a));
    }
}
