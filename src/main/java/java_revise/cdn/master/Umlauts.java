package java_revise.cdn.master;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@UtilityClass
@Slf4j
public class Umlauts {
	
	/*
	oóyÿeêssßzž
	 */
	
	private final String[][] UMLAUT_REPLACEMENTS = new String[][]{
			{"ä", "ae"},
			{"ü", "ue"},
			{"ö", "oe"},
			{"ß", "ss"},
			{"ç", "c"},
			{"ô", "o"},
			{"ñ", "n"},
			{"ë", "e"},
			{"š", "s"},
			{"đ", "d"},
			{"å", "a"},
			{"ã", "a"},
			{"ø", "o"},
			{"æ", "ae"},
			{"ñ", "n"},
			{"á", "a"},
			{"í", "i"},
			{"uú", ""},
			{"ó", "o"},
			{"ÿ", "y"},
			{"ê", "e"},
			{"ß", "ss"},
			{"ž", "z"}
	};
	public String replaceUmlaute(String value) {
		String result = value;
		for (String[] umlautReplacement : UMLAUT_REPLACEMENTS) {
			result = StringUtils.replace(result, umlautReplacement[0], umlautReplacement[1]);
			result = StringUtils.replace(result, StringUtils.capitalize(umlautReplacement[0]), StringUtils.capitalize(umlautReplacement[1]));
		}
		return result;
	}
	
}

