package java_revise.cdn.master;

import java.util.Scanner;

public class Staircase {
	
	static void staircase(int n, String icon) {
		String str = "#";
		if(icon != null || !icon.trim().isEmpty()){
			str = icon;
		}
		for(int i = 1; i <= n; i++){
			System.out.printf("%" + (n) + "s \n", str);
			str += icon;
		}
		
	}
	
	private static final Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		int n = scanner.nextInt();
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
		
		staircase(n, "&");
		
		scanner.close();
		
	}
}
