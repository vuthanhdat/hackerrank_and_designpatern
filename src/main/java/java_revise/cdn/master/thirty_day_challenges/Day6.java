package java_revise.cdn.master.thirty_day_challenges;

import java.util.*;

public class Day6 {

    static void printEvenAndOddChars(String input){
        String evenChars = "";
        String oddChars = "";
        for(int i =0; i < input.length(); i++){
            if(i % 2 == 0){
                evenChars += input.charAt(i);
            } else {
                oddChars += input.charAt(i);
            }
        }
        System.out.println(evenChars + " " + oddChars);
    }

    static void printEvenAndOddCharsFromArray(String[] inputs){
        for (int i = 0; i < inputs.length; i++){
            printEvenAndOddChars(inputs[i]);
        }
    }

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
        String[] inputs = new String[n];
        for(int i = 0; i < n; i++){
            inputs[i] = scanner.nextLine();

        }
        printEvenAndOddCharsFromArray(inputs);
        scanner.close();
    }
}

