package java_revise.cdn.master.thirty_day_challenges;

import java.util.*;

public class Day10 {

    static void countNumber1InBin(int number) {
        int i = 0;
        while (number!=0)
        {
            // This operation reduces length
            // of every sequence of 1s by one.
            number = (number & (number << 1));
            i++;
        }
        System.out.println(i);
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
        countNumber1InBin(n);
        scanner.close();
    }
}
