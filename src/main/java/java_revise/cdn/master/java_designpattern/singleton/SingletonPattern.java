package java_revise.cdn.master.java_designpattern.singleton;

import java.util.Objects;

public class SingletonPattern {
    private static volatile SingletonPattern instance = null;

    private SingletonPattern() {}

    public static SingletonPattern getSingleInstance() {
        if (instance == null) {
            synchronized (SingletonPattern.class) {
                if (Objects.isNull(instance)) {
                    instance = new SingletonPattern();
                }
            }
        }
        return instance;
    }
}
