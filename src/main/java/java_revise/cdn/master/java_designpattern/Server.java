package java_revise.cdn.master.java_designpattern;

import lombok.Data;

public class Server extends Computer {

    private String ram;
    private String cpu;
    private String drive;

    public Server(String ram, String cpu, String drive){
        this.ram = ram;
        this.cpu = cpu;
        this.drive = drive;
    }


    @Override
    public String getRAM() {
        return this.ram;
    }

    @Override
    public String getDrive() {
        return this.drive;
    }

    @Override
    public String getCPU() {
        return this.cpu;
    }
}
