package java_revise.cdn.master.java_designpattern;

public abstract class Computer {
    public abstract String getRAM();
    public abstract String getDrive();
    public abstract String getCPU();

    @Override
    public String toString(){
        return "RAM = " + getRAM() + " ,CPU = " + getCPU() + " Drive = " + getDrive();
    }
}
