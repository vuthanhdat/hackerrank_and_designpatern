package java_revise.cdn.master.java_designpattern.abstractfactory;

import com.sun.istack.internal.NotNull;
import java_revise.cdn.master.java_designpattern.Computer;

public class ComputerFactory implements IComputerAbstractFactory {
    public static Computer getComputer(@NotNull IComputerAbstractFactory factory){
        return factory.createComputer();
    }
    
    @Override
    public Computer createComputer() {
        return null;
    }
}
