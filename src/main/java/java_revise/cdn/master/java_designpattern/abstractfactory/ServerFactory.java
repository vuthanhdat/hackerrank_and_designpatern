package java_revise.cdn.master.java_designpattern.abstractfactory;

import java_revise.cdn.master.java_designpattern.Server;
import java_revise.cdn.master.java_designpattern.Computer;

public class ServerFactory implements IComputerAbstractFactory {

    private String ram;
    private String cpu;
    private String drive;

    public ServerFactory(String ram, String cpu, String drive){
        this.ram = ram;
        this.cpu = cpu;
        this.drive = drive;
    }

    public Computer createComputer() {
        return new Server(ram, cpu, drive);
    }
}
