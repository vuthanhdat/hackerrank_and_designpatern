package java_revise.cdn.master.java_designpattern.abstractfactory;

import java_revise.cdn.master.java_designpattern.Computer;
import java_revise.cdn.master.java_designpattern.PC;

public class PCFactory implements IComputerAbstractFactory {

    private String ram;
    private String cpu;
    private String drive;

    public PCFactory(String ram, String cpu, String drive){
        this.ram = ram;
        this.cpu = cpu;
        this.drive = drive;
    }

    public Computer createComputer() {
        return new PC(ram, cpu, drive);
    }
}
