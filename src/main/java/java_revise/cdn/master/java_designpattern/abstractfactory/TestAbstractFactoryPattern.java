package java_revise.cdn.master.java_designpattern.abstractfactory;

import java_revise.cdn.master.java_designpattern.Computer;

public class TestAbstractFactoryPattern {
    public static void main(String[] args) {
        testAbstractFactory();
    }

    private static void testAbstractFactory() {
        Computer pc = ComputerFactory.getComputer(new PCFactory("2 GB","2.4 GHz", "500 GB"));
        Computer server = ComputerFactory.getComputer(new ServerFactory("16 GB", "2.9 GHz", "1 TB"));
        System.out.println("AbstractFactory PC Config::" + pc);
        System.out.println("AbstractFactory Server Config::" + server);
    }

}
