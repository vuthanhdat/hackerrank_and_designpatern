package java_revise.cdn.master.java_designpattern.abstractfactory;

import java_revise.cdn.master.java_designpattern.Computer;

public interface IComputerAbstractFactory {
    public Computer createComputer();
}
