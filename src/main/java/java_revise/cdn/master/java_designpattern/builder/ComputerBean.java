package java_revise.cdn.master.java_designpattern.builder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class ComputerBean {
    private String ram;
    private String cpu;
    private String drive;
}
