package java_revise.cdn.master.java_designpattern.builder;

public abstract class ComputerBuilder {
    protected ComputerBean computer;

    public ComputerBuilder() {
        computer = new ComputerBean();
    }

    public abstract ComputerBuilder setRAM(String ram);
    public abstract ComputerBuilder setDrive(String drive);
    public abstract ComputerBuilder setCPU(String cpu);
    public abstract ComputerBean build();
}
