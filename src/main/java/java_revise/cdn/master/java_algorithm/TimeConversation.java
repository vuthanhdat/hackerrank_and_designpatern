package java_revise.cdn.master.java_algorithm;

public class TimeConversation {

    static String convertTime(String s) {

        String convertedTime = "";

        try {
            String[] timeArray = s.split(":");

            int hour = Integer.parseInt(timeArray[0]);

            String min = timeArray[1];
            String secWithPeriod = timeArray[2];
            String sec = secWithPeriod.substring(0, secWithPeriod.length() - 2);
            String period = secWithPeriod.substring(secWithPeriod.length() - 2);


            if(period.equalsIgnoreCase("AM") && (0 <= hour && hour < 12)){
                convertedTime = String.format("%02d", hour) + ":" + min + ":" + sec;
            } else if(period.equalsIgnoreCase("PM") && (0 <= hour && hour < 12)){
                convertedTime = hour + 12 + ":" + min + ":" + sec;
            } else if (hour == 12 && period.equalsIgnoreCase("AM")){
                convertedTime = "00:" + min + ":" + sec;
            } else if(hour == 12 && period.equalsIgnoreCase("PM")) {
                convertedTime = hour + ":" + min + ":" + sec;
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return convertedTime;
    }

    public static void main(String[] args) {
        System.out.println(convertTime("12:05:01PM"));
    }
}
