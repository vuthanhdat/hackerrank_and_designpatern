package java_revise.cdn.master.interview;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

// queue node used in BFS
class ChessPoint {
    // (x, y) represents chess board coordinates
    // dist represent its minimum distance from the source
    int x;
    int y;
    int dist;

    public ChessPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public ChessPoint(int x, int y, int dist) {
        this.x = x;
        this.y = y;
        this.dist = dist;
    }

    // As we are using class object as a key in a HashMap
    // we need to implement hashCode() and equals()

    // -- below methods are generated by IntelliJ IDEA (Alt + Insert) --
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChessPoint node = (ChessPoint) o;

        if (x != node.x) return false;
        if (y != node.y) return false;
        return dist == node.dist;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        result = 31 * result + dist;
        return result;
    }
};

class ChessKnight {
    // Below arrays details all 8 possible movements
    // for a knight
    private static int row[] = {2, 2, -2, -2, 1, 1, -1, -1};
    private static int col[] = {-1, 1, 1, -1, 2, -2, 2, -2};

    // Check if (x, y) is valid chess board coordinates
    // Note that a knight cannot go out of the chessboard
    private static boolean valid(int x, int y, int N) {
        if (x < 0 || y < 0 || x >= N || y >= N)
            return false;

        return true;
    }

    // Find minimum number of steps taken by the knight
    // from source to reach destination using BFS
    public static int BFS(ChessPoint src, ChessPoint dest, int N) {
        // map to check if matrix cell is visited before or not
        Map<ChessPoint, Boolean> visited = new HashMap<ChessPoint, Boolean>();

        // create a queue and enqueue first node
        Queue<ChessPoint> q = new ArrayDeque<ChessPoint>();
        q.add(src);

        // run till queue is not empty
        while (!q.isEmpty()) {
            // pop front node from queue and process it
            ChessPoint node = q.poll();

            int x = node.x;
            int y = node.y;
            int dist = node.dist;

            // if destination is reached, return distance
            if (x == dest.x && y == dest.y)
                return dist;

            // Skip if location is visited before
            if (visited.get(node) == null) {
                // mark current node as visited
                visited.put(node, true);

                // check for all 8 possible movements for a knight
                // and enqueue each valid movement into the queue
                for (int i = 0; i < 8; ++i) {
                    // Get the new valid position of Knight from current
                    // position on chessboard and enqueue it in the
                    // queue with +1 distance
                    int x1 = x + row[i];
                    int y1 = y + col[i];

                    if (valid(x1, y1, N))
                        q.add(new ChessPoint(x1, y1, dist + 1));
                }
            }
        }

        // return INFINITY if path is not possible
        return Integer.MAX_VALUE;
    }

    public static void main(String[] args) {
        int N = 8;

        // source coordinates
        ChessPoint src = new ChessPoint(0, 0);

        // destination coordinates
        ChessPoint dest = new ChessPoint(4, 4);

        System.out.println("Minimum number of steps required is " +
                BFS(src, dest, N));
    }
}
